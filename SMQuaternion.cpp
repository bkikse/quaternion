/*
 * Copyright (c) 2007-2017, KI IN Bang
 * All rights reserved.
 * SPATIAL&SPATIAL-MATICS for EarthOnAir
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "SMQuaternion.h"

//#define smallnumber 1.0e-11
//#define M_PI 3.14159265358979

namespace EOA
{
	/// constructors
	CSMQuaternion::CSMQuaternion() { identity(); }
	CSMQuaternion::CSMQuaternion(const double w, const double x, const double y, const double z) { this->w = w; this->x = x; this->y = y; this->z = z; }
	/// constructor for a 3D vector
	CSMQuaternion::CSMQuaternion(const double x, const double y, const double z) { this->w = 0.0; this->x = x; this->y = y; this->z = z; }
	/// constructor from a rotation matrix
	CSMQuaternion::CSMQuaternion(const CSMMatrix<double>& R) { Rotation::EulerToQuaternion(R, w, x, y, z); }
	/// constructor for rotation axis and angle
	CSMQuaternion::CSMQuaternion(const CSMQuaternion& V, const double th) { w = cos(th*0.5); x = V.x*sin(th*0.5); y = V.y*sin(th*0.5); z = V.z*sin(th*0.5); }
	/// copy constructor
	CSMQuaternion::CSMQuaternion(const CSMQuaternion& copy) { w = copy.w; x = copy.x; y = copy.y; z = copy.z; }
	/// destructor
	CSMQuaternion::~CSMQuaternion() {}

	/// assignment operator
	void CSMQuaternion::operator = (const CSMQuaternion& copy) { w = copy.w; x = copy.x; y = copy.y; z = copy.z; }

	/// quaternion multiplication (Hamilton product) operator
	CSMQuaternion CSMQuaternion::operator % (const CSMQuaternion& q) const
	{
		// quaternion: {w, v(x,y,z)}
		// p%q = ( p.w*q.w - p.v*q.v, p.w*q.v + p.v * q.w + p.v X q.v )  
		// p%q = ( p.w*q.w - p(xyz) dot q(xyz), p.w*q(xyz) 
		//                                    + p(xyz) * q.w 
		//                                    + p(xyz) cross q(xyz) ) 
		CSMQuaternion ret;

		ret.w = w*q.w - (x*q.x + y*q.y + z*q.z);
		ret.x = w*q.x + x*q.w + (y*q.z - z*q.y);
		ret.y = w*q.y + y*q.w + (z*q.x - x*q.z);
		ret.z = w*q.z + z*q.w + (x*q.y - y*q.x);

		return ret;
	}

	/// quaternion multiplication (Hamilton product) and assignment operator
	CSMQuaternion& CSMQuaternion::operator %= (const CSMQuaternion& q)
	{
		*this = this->operator%(q);
		return *this;
	}

	/// scalar multiplication
	CSMQuaternion CSMQuaternion::operator * (const double& val) const
	{
		CSMQuaternion ret;

		ret.w = this->w*val;
		ret.x = this->x*val;
		ret.y = this->y*val;
		ret.z = this->z*val;

		return ret;
	}

	/// quaternion division (non-commutative) operator
	CSMQuaternion CSMQuaternion::operator / (const CSMQuaternion& q) const
	{
		// p/q = p % (q_conjugate/q.norm^2)
		// or 
		// p/q = q_conjugate % (p/q.norm^2)
		// p/q = ( p.w*q.w + p(xyz) dot q(xyz),- p.w*q(xyz) 
		//                                     + p(xyz) * q.w 
		//                                     - p(xyz) cross q(xyz) ) 
		CSMQuaternion ret;
		try
		{
			//ret = q.Conjugate() % (p / q.Norm2());
			ret = (*this) % (q.Conjugate() / q.Norm2());
		}
		catch (...)
		{
			std::string msg("Error: operator / (const CSMQuaternion& q) in SMQuaternion.h");
			std::cerr << msg;
			throw std::runtime_error(msg);
		}

		return ret;
	}

	/// quaternion division (non-commutative) and assignment operator
	CSMQuaternion CSMQuaternion::operator /= (const CSMQuaternion& q)
	{
		*this = this->operator/(q);
		return *this;
	}

	/// scalar multiplication and assignment operator
	CSMQuaternion& CSMQuaternion::operator *= (const double& val)
	{
		if (val == 0)
			throw CSMQuaternionError(_DIVIDED_ZERO_);

		this->w *= val;
		this->x *= val;
		this->y *= val;
		this->z *= val;

		return *this;
	}

	/// quaternion dot (inner) product operator
	double CSMQuaternion::operator * (const CSMQuaternion& val) const
	{
		return (this->w*val.w + this->x*val.x + this->y*val.y + this->z*val.z);
	}

	/// scalar division operator
	CSMQuaternion CSMQuaternion::operator / (const double& val) const
	{
		if (val == 0)
			throw CSMQuaternionError(_DIVIDED_ZERO_);

		CSMQuaternion ret;

		ret.w = this->w / val;
		ret.x = this->x / val;
		ret.y = this->y / val;
		ret.z = this->z / val;

		return ret;
	}

	/// scalar division and assignment operator
	CSMQuaternion& CSMQuaternion::operator /= (const double& val)
	{
		if (val == 0)
			throw CSMQuaternionError(_DIVIDED_ZERO_);

		this->w /= val;
		this->x /= val;
		this->y /= val;
		this->z /= val;

		return *this;
	}

	/// quaternion addition operator
	CSMQuaternion CSMQuaternion::operator + (const CSMQuaternion& q) const
	{
		//v(x,y,z)
		//p+q = ( p.w+q.w, p.x+q.x, p.y+q.y, p.z+q.z)  
		CSMQuaternion ret;

		ret.w = this->w + q.w;
		ret.x = this->x + q.x;
		ret.y = this->y + q.y;
		ret.z = this->z + q.z;

		return ret;
	}

	/// quaternion addition and assignment operator
	CSMQuaternion& CSMQuaternion::operator += (const CSMQuaternion& q)
	{
		//*this = this->operator + (q);
		this->w += q.w;
		this->x += q.x;
		this->y += q.y;
		this->z += q.z;

		return *this;
	}

	/// quaternion substraction operator
	CSMQuaternion CSMQuaternion::operator - (const CSMQuaternion& q) const
	{
		CSMQuaternion ret;

		ret.w = this->w - q.w;
		ret.x = this->x - q.x;
		ret.y = this->y - q.y;
		ret.z = this->z - q.z;

		return ret;
	}

	/// quaternion substraction and assignment operator
	CSMQuaternion& CSMQuaternion::operator -= (const CSMQuaternion& q)
	{
		this->w -= q.w;
		this->x -= q.x;
		this->y -= q.y;
		this->z -= q.z;

		return *this;
	}

	/// minus(-) sign operator
	CSMQuaternion CSMQuaternion::operator - () const
	{
		CSMQuaternion ret = *this * (-1.0);
		return ret;
	}

	double& CSMQuaternion::qw() { return this->w; }
	double& CSMQuaternion::qx() { return this->x; }
	double& CSMQuaternion::qy() { return this->y; }
	double& CSMQuaternion::qz() { return this->z; }
	void CSMQuaternion::reset(const double qw, const double qx, const double qy, const double qz)
	{
		w = qw;
		x = qx;
		y = qy;
		z = qz;
	}

	/// conjugate quaternion
	CSMQuaternion CSMQuaternion::Conjugate() const
	{
		CSMQuaternion ret(*this);

		ret.x *= -1.0;
		ret.y *= -1.0;
		ret.z *= -1.0;

		return ret;
	}

	/// 3D rotation
	CSMQuaternion CSMQuaternion::Rotate(const CSMQuaternion P) const
	{
		if (P.w != 0)
			throw CSMQuaternionError(_NOT_POINT_);

		return Rotate(P.x, P.y, P.z);
	}

	/// 3D rotation
	CSMQuaternion CSMQuaternion::Rotate(const double ax, const double ay, const double az) const
	{
		double norm = Norm();

		if (norm == 0)
			throw CSMQuaternionError(_DIVIDED_ZERO_);

		double w_, x_, y_, z_;

		w_ = w / norm;
		x_ = x / norm;
		y_ = y / norm;
		z_ = z / norm;

		CSMQuaternion ret;

		//
		//R % P % (R.Conjugate())
		//(w,x,y,z) % (0,ax,ay,az) % (w,-x,-y,-z)
		//
		ret.w = 0;
		ret.x = ax*(w_*w_ + x_*x_ - y_*y_ - z_*z_) - 2 * ay*w_*z_ + 2 * ay*x_*y_ + 2 * az*w_*y_ + 2 * az*x_*z_;
		ret.y = ay*(w_*w_ - x_*x_ + y_*y_ - z_*z_) + 2 * ax*w_*z_ + 2 * ax*x_*y_ - 2 * az*w_*x_ + 2 * az*y_*z_;
		ret.z = az*(w_*w_ - x_*x_ - y_*y_ + z_*z_) - 2 * ax*w_*y_ + 2 * ax*x_*z_ + 2 * ay*w_*x_ + 2 * ay*y_*z_;

		return ret;
	}

	/// Partial derivatives of quaternion rotation w.r.t. "w"
	CSMQuaternion CSMQuaternion::Rotate_dw(const double& ax, const double& ay, const double& az) const
	{
		double norm = Norm();

		if (norm == 0)
			throw CSMQuaternionError(_DIVIDED_ZERO_);

		double w_, x_, y_, z_;

		w_ = w / norm;
		x_ = x / norm;
		y_ = y / norm;
		z_ = z / norm;

		CSMQuaternion ret;

		//
		//(w,x,y,z) % (0,ax,ay,az) % (w,-x,-y,-z)
		//
		ret.w = 0;
		ret.x = ax*(2 * w_) - 2 * ay*z_ + 2 * az*y_;
		ret.y = ay*(2 * w_) + 2 * ax*z_ - 2 * az*x_;
		ret.z = az*(2 * w_) - 2 * ax*y_ + 2 * ay*x_;

		return ret;
	}

	/// Partial derivatives of quaternion rotation w.r.t. "x"
	CSMQuaternion CSMQuaternion::Rotate_dx(const double& ax, const double& ay, const double& az) const
	{
		double norm = Norm();

		if (norm == 0)
			throw CSMQuaternionError(_DIVIDED_ZERO_);

		double w_, x_, y_, z_;

		w_ = w / norm;
		x_ = x / norm;
		y_ = y / norm;
		z_ = z / norm;

		CSMQuaternion ret;

		//
		//(w,x,y,z) % (0,ax,ay,az) % (w,-x,-y,-z)
		//
		ret.w = 0;
		ret.x = ax*(2 * x_) - 2 * ay*y_ + 2 * az*z_;
		ret.y = ay*(-2 * x_) + 2 * ax*y_ - 2 * az*w_;
		ret.z = az*(-2 * x_) + 2 * ax*z_ + 2 * ay*w_;

		return ret;
	}

	/// Partial derivatives of quaternion rotation w.r.t. "y"
	CSMQuaternion CSMQuaternion::Rotate_dy(const double& ax, const double& ay, const double& az) const
	{
		double norm = Norm();

		if (norm == 0)
			throw CSMQuaternionError(_DIVIDED_ZERO_);

		double w_, x_, y_, z_;

		w_ = w / norm;
		x_ = x / norm;
		y_ = y / norm;
		z_ = z / norm;

		CSMQuaternion ret;

		//
		//(w,x,y,z) % (0,ax,ay,az) % (w,-x,-y,-z)
		//
		ret.w = 0;
		ret.x = ax*(-2 * y_) + 2 * ay*x_ + 2 * az*w_;
		ret.y = ay*(2 * y_) + 2 * ax*x_ + 2 * az*z_;
		ret.z = az*(-2 * y_) - 2 * ax*w_ + 2 * ay*z_;

		return ret;
	}

	/// Partial derivatives of quaternion rotation w.r.t. "z"
	CSMQuaternion CSMQuaternion::Rotate_dz(const double& ax, const double& ay, const double& az) const
	{
		double norm = Norm();

		if (norm == 0)
			throw CSMQuaternionError(_DIVIDED_ZERO_);

		double w_, x_, y_, z_;

		w_ = w / norm;
		x_ = x / norm;
		y_ = y / norm;
		z_ = z / norm;

		CSMQuaternion ret;

		//
		//(w,x,y,z) % (0,ax,ay,az) % (w,-x,-y,-z)
		//
		ret.w = 0;
		ret.x = ax*(-2 * z_) - 2 * ay*w_ + 2 * az*x_;
		ret.y = ay*(-2 * z_) + 2 * ax*w_ + 2 * az*y_;
		ret.z = az*(+2 * z_) + 2 * ax*x_ + 2 * ay*y_;

		return ret;
	}

	/// sum of squares
	double CSMQuaternion::Norm2() const
	{
		return (w*w + x*x + y*y + z*z);
	}

	/// norm
	double CSMQuaternion::Norm() const
	{
		return sqrt(Norm2());
	}

	/// normalization (unit quaternion)
	void CSMQuaternion::Normalize()
	{
		double norm = Norm();
		if (0.0 == norm)
		{
			throw CSMQuaternionError(_DIVIDED_ZERO_);
		}
		else
		{
			if (1.0 != norm) *this /= norm;
		}
	}

	/// multiplicative identity quaternion
	/// (1, 0, 0, 0)%(w,x,y,z) = (w,x,y,z)
	/// (w,x,y,z)%(1, 0, 0, 0) = (w,x,y,z)
	void CSMQuaternion::identity()
	{
		w = 1.0;
		x = y = z = 0.0;
	}

	namespace Rotation
	{
		/// quaternion to rotation matrix
		CSMMatrix<double> QuaternionToEuler(const double w, const double x, const double y, const double z)
		{
			CSMMatrix<double> R;

			try
			{
				double norm = sqrt(w*w + x*x + y*y + z*z);
				R = UnitQuaternionToEuler(w / norm, x / norm, y / norm, z / norm);
			}
			catch (...)
			{
				std::string msg("Error: QuaternionToEuler(const double w, const double x, const double y, const double z) in SMQuaternion.h");
				std::cerr << msg;
				throw std::runtime_error(msg);
			}

			return R;
		}

		/// unit quaternion to rotation matrix
		CSMMatrix<double> UnitQuaternionToEuler(const double q0, const double q1, const double q2, const double q3)
		{
			CSMMatrix<double> R(3, 3);

			R(0, 0) = 1.0 - 2.0 * q2*q2 - 2 * q3*q3;	R(0, 1) = 2.0 * q1*q2 - 2.0 * q0*q3;		R(0, 2) = 2.0 * q1*q3 + 2.0 * q0*q2;
			R(1, 0) = 2.0 * q1*q2 + 2.0 * q0*q3;		R(1, 1) = 1.0 - 2.0 * q1*q1 - 2.0 * q3*q3;	R(1, 2) = 2.0 * q2*q3 - 2.0 * q0*q1;
			R(2, 0) = 2.0 * q1*q3 - 2.0 * q0*q2;		R(2, 1) = 2.0 * q2*q3 + 2.0 * q0*q1;		R(2, 2) = 1.0 - 2.0 * q1*q1 - 2.0 * q2*q2;

			return R;
		}

		/// quaternion to Euler angles
		/// it depends on the order of rotations.
		void quatToEulerAngles(const double q0, const double q1, const double q2, const double q3, double& ang1, double& ang2, double& ang3)
		{
			//https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

			//Angles are depend on rotation configuration.
			//This function provides three euler angles defined in Wikipedia quaternion.
			//It is better transform between 4 quaternion numbers and 9 3by3 matrix, rather than 3 euler angles.
			//Because of ambiguity in 3 euler angles.
			//
			// Note: the following codes are for determining Euler angles based on the rotation definition below
			// R3(ang3) % R2(ang2) % R1(ang1)
			//
			// R(0, 0) = cos(ang2)cos(ang3);	R(0, 1) = -cos(ang1)sin(ang3) + sin(ang1)sin(ang2)cos(ang3);	R(0, 2) = sin(ang1)sin(ang3) + cos(ang1)sin(ang2)cos(ang3);
			// R(1, 0) = cos(ang2)sin(ang3);	R(1, 1) = cos(ang1)cos(ang3) + sin(ang1)sin(ang2)sin(ang3);		R(1, 2) = -sin(ang1)cos(ang3) + cos(ang1)sin(ang2)sin(ang3);
			// R(2, 0) = -sin(ang2);			R(2, 1) = sin(ang1)cos(ang2);									R(2, 2) = cos(ang1)COS(ang2);

			// ang1 = atan2(R(2,1), R(2,2)) = atan2(sin(ang1)cos(ang2), cos(ang1)cos(ang2));
			// ang2 = asin(-R(2,0)) = -2.0 * q1*q3 + 2.0 * q0*q2
			// ang3 = atan2(R(1,0), R(0, 0)) = atan2(cos(ang2)sin(ang3), cos(ang2)cos(ang3))

			double sinAng1 = 2.0 * (q0 * q1 + q2 * q3);
			double cosAng1 = 1.0 - 2.0 * (q1 * q1 + q2 * q2);
			ang1 = atan2(sinAng1, cosAng1);

			double sinAng2 = 2.0 * (q0 * q2 - q3 * q1);

			if (sinAng2 >= 1.0)		ang2 = M_PI_2;
			else if (sinAng2 <= -1.0)		ang2 = -M_PI_2;
			else							ang2 = asin(sinAng2);

			double sinAng3 = 2.0 * (q0 * q3 + q1 * q2);
			double cosAng3 = 1.0 - 2.0 * (q2 * q2 + q3 * q3);
			ang3 = atan2(sinAng3, cosAng3);
		}

		/// spherical quaternion interpolation
		CSMQuaternion Interpolation(const CSMQuaternion& qa, const CSMQuaternion& qb, const double ta, const double tb, const double t, const double th)
		{
			double dt = tb - ta;
			if (dt == 0. || t == ta)
				return qa;
			else if (t == tb)
				return qb;

			return Interpolation(qa, qb, (t - ta) / (dt), th);
		}

		/// quaternion interpolation: slerp and lerp
		CSMQuaternion Interpolation(const CSMQuaternion& qa, const CSMQuaternion& qb0, const double u, const double th)
		{
			// 0 < |u| < 1.0

			CSMQuaternion qb2;
			double dot = qa * qb0;

			CSMQuaternion retval;
			//	
			//dot is cos(theta),
			//if (dot < 0), q1 and q2 are more than 90 degrees apart,
			//so we can invert one to reduce spinning
			//
			CSMQuaternion qb;
			if (fabs(dot) < th)// spherical interpolation (slerp)
			{
				if (dot < 0.0)
				{
					dot = dot * -1.0;
					qb = qb0 * -1.0;
				}

				double angle = acos(dot);
				retval = (qa*sin(angle*(1.0 - u)) + qb*sin(angle*u)) / sin(angle);
			}
			else//linear interpolartion (lerp)
			{
				retval = (qa*(1.0 - u) + qb*u);
			}

			retval.Normalize();
			return retval;
		}

		/// interpolation: slerp
		CSMQuaternion InterpolationSlerp(const CSMQuaternion& qa, const CSMQuaternion& qb0, const double dot0, const double u)
		{
			// 0 < |u| < 1.0

			//acos: 0.0 to 1.0 (therefore, -pi/2 to +pi/2)
			//if dot(qa*qb) is less than zero, then make it positive.
			double dot = dot0;
			CSMQuaternion qb = qb0;
			if (dot0 < 0.0)
			{
				dot = dot * -1.0;
				qb = qb * -1.0;;
			}
			double angle = acos(dot);
			CSMQuaternion retval = (qa*sin(angle*(1.0 - u)) + qb*sin(angle*u)) / sin(angle);
			retval.Normalize();
			return retval;
		}

		/// lerp quaternion interpolation
		CSMQuaternion InterpolationLerp(const CSMQuaternion& qa, const CSMQuaternion& qb, const double u)
		{
			CSMQuaternion retval = (qa*(1.0 - u) + qb*u);
			retval.Normalize();
			return retval;
		}

		/// quaternion interpolation
		/// it does not check > 90 deg
		CSMQuaternion InterpolationNoInvert(const CSMQuaternion& qa, const CSMQuaternion& qb, const double u, const double th)
		{
			double dot = qa * qb;
			CSMQuaternion retval;
			if (fabs(dot) < th)
			{
				double angle = acos(dot);
				retval = (qa*sin(angle*(1.0 - u)) + qb*sin(angle*u)) / sin(angle);
			}
			else  // if the angle is small, use linear interpolation								
				retval = (qa*(1.0 - u) + qb*u);

			retval.Normalize();
			return retval;
		}

		/// Error propagation for quaternion derived from Euler angles
		CSMQuaternion EulerToQuaternion_Variance(const double omega, const double phi, const double kappa, const double VarO, const double VarP, const double VarK)
		{
			CSMMatrix<double> RmatVar = GetRmatVarfromEulerAngleVar(omega, phi, kappa, VarO, VarP, VarK);

			double w, x, y, z;

			_Rmat_ Rotation(omega, phi, kappa);
			CSMMatrix<double> R = Rotation.rmatrix();
			double qw, qx, qy, qz;

			w = 0.5*sqrt(R(0, 0) + R(1, 1) + R(2, 2) + 1.0);

			if (sqrt(R(0, 0) + R(1, 1) + R(2, 2) + 1.0) > 0.0)
			{
				CSMMatrix<double> G(1, 3, 0);
				CSMMatrix<double> V(3, 3, 0);

				V(0, 0) = RmatVar(0, 0);
				V(1, 1) = RmatVar(1, 1);
				V(2, 2) = RmatVar(2, 2);

				G(0, 0) = 0.5*0.5 / sqrt(R(0, 0) + R(1, 1) + R(2, 2) + 1.0);
				G(0, 1) = 0.5*0.5 / sqrt(R(0, 0) + R(1, 1) + R(2, 2) + 1.0);
				G(0, 2) = 0.5*0.5 / sqrt(R(0, 0) + R(1, 1) + R(2, 2) + 1.0);

				qw = (G%V%G.Transpose())(0, 0);
			}

			if (fabs(w) > fabs(smallnumber))
			{
				CSMMatrix<double> G(1, 3, 0);
				CSMMatrix<double> V(3, 3, 0);

				//x = (0.25/w)*(R(2,1) - R(1,2));
				V(0, 0) = qw;
				V(1, 1) = RmatVar(2, 1);
				V(2, 2) = RmatVar(1, 2);

				G(0, 0) = -0.25 / w / w*(R(2, 1) - R(1, 2));
				G(0, 1) = 0.25 / w;
				G(0, 2) = -0.25 / w;

				qx = (G%V%G.Transpose())(0, 0);

				//y = (0.25/w)*(R(0,2) - R(2,0));
				V(0, 0) = qw;
				V(1, 1) = RmatVar(0, 2);
				V(2, 2) = RmatVar(2, 0);

				G(0, 0) = -0.25 / w / w*(R(0, 2) - R(2, 0));
				G(0, 1) = 0.25 / w;
				G(0, 2) = -0.25 / w;

				qy = (G%V%G.Transpose())(0, 0);

				//z = (0.25/w)*(R(1,0) - R(0,1));
				V(0, 0) = qw;
				V(1, 1) = RmatVar(1, 0);
				V(2, 2) = RmatVar(0, 1);

				G(0, 0) = -0.25 / w / w*(R(1, 0) - R(0, 1));
				G(0, 1) = 0.25 / w;
				G(0, 2) = -0.25 / w;

				qz = (G%V%G.Transpose())(0, 0);
			}
			else
			{
				x = 0.5*sqrt(R(0, 0) - R(1, 1) - R(2, 2) + 1.0);
				y = 0.5*sqrt(-R(0, 0) + R(1, 1) - R(2, 2) + 1.0);
				z = 0.5*sqrt(-R(0, 0) - R(1, 1) + R(2, 2) + 1.0);

				if (fabs(x) >= fabs(y) && fabs(x) >= fabs(z) && fabs(x) > fabs(smallnumber))
				{
					if (sqrt(R(0, 0) - R(1, 1) - R(2, 2) + 1.0) > 0.0)
					{
						CSMMatrix<double> G(1, 3, 0);
						CSMMatrix<double> V(3, 3, 0);

						V(0, 0) = RmatVar(0, 0);
						V(1, 1) = RmatVar(1, 1);
						V(2, 2) = RmatVar(2, 2);

						G(0, 0) = 0.5*0.5 / sqrt(R(0, 0) - R(1, 1) - R(2, 2) + 1.0);
						G(0, 1) = -0.5*0.5 / sqrt(R(0, 0) - R(1, 1) - R(2, 2) + 1.0);
						G(0, 2) = -0.5*0.5 / sqrt(R(0, 0) - R(1, 1) - R(2, 2) + 1.0);

						qx = (G%V%G.Transpose())(0, 0);
					}

					CSMMatrix<double> G(1, 3, 0);
					CSMMatrix<double> V(3, 3, 0);

					//y = (0.25/x)*(R(0,1) + R(1,0));
					V(0, 0) = qx;
					V(1, 1) = RmatVar(0, 1);
					V(2, 2) = RmatVar(1, 0);

					G(0, 0) = -0.25 / x / x*(R(0, 1) + R(1, 0));
					G(0, 1) = 0.25 / x;
					G(0, 2) = 0.25 / x;

					qy = (G%V%G.Transpose())(0, 0);

					//z = (0.25/x)*(R(2,0) + R(0,2));
					V(0, 0) = qx;
					V(1, 1) = RmatVar(2, 0);
					V(2, 2) = RmatVar(0, 2);

					G(0, 0) = -0.25 / x / x*(R(2, 0) + R(0, 2));
					G(0, 1) = 0.25 / x;
					G(0, 2) = 0.25 / x;

					qz = (G%V%G.Transpose())(0, 0);

					//w = (0.25/x)*(R(2,1) - R(1,2));
					V(0, 0) = qx;
					V(1, 1) = RmatVar(2, 1);
					V(2, 2) = RmatVar(1, 2);

					G(0, 0) = -0.25 / x / x*(R(2, 1) - R(1, 2));
					G(0, 1) = 0.25 / x;
					G(0, 2) = -0.25 / x;

					qw = (G%V%G.Transpose())(0, 0);
				}
				else if (fabs(y) >= fabs(x) && fabs(y) >= fabs(z) && fabs(y) > fabs(smallnumber))
				{
					if (sqrt(-R(0, 0) + R(1, 1) - R(2, 2) + 1.0) > 0.0)
					{
						CSMMatrix<double> G(1, 3, 0);
						CSMMatrix<double> V(3, 3, 0);

						V(0, 0) = RmatVar(0, 0);
						V(1, 1) = RmatVar(1, 1);
						V(2, 2) = RmatVar(2, 2);

						G(0, 0) = -0.5*0.5 / sqrt(-R(0, 0) + R(1, 1) - R(2, 2) + 1.0);
						G(0, 1) = 0.5*0.5 / sqrt(-R(0, 0) + R(1, 1) - R(2, 2) + 1.0);
						G(0, 2) = -0.5*0.5 / sqrt(-R(0, 0) + R(1, 1) - R(2, 2) + 1.0);

						qy = (G%V%G.Transpose())(0, 0);
					}

					CSMMatrix<double> G(1, 3, 0);
					CSMMatrix<double> V(3, 3, 0);

					//x = (0.25/y)*(R(0,1) + R(1,0));
					V(0, 0) = qy;
					V(1, 1) = RmatVar(0, 1);
					V(2, 2) = RmatVar(1, 0);

					G(0, 0) = -0.25 / y / y*(R(0, 1) + R(1, 0));
					G(0, 1) = 0.25 / y;
					G(0, 2) = 0.25 / y;

					qx = (G%V%G.Transpose())(0, 0);

					//z = (0.25/y)*(R(1,2) + R(2,1));
					V(0, 0) = qy;
					V(1, 1) = RmatVar(1, 2);
					V(2, 2) = RmatVar(2, 1);

					G(0, 0) = -0.25 / y / y*(R(1, 2) + R(2, 1));
					G(0, 1) = 0.25 / y;
					G(0, 2) = 0.25 / y;

					qz = (G%V%G.Transpose())(0, 0);

					//w = (0.25/y)*(R(0,2) - R(2,0));
					V(0, 0) = qy;
					V(1, 1) = RmatVar(0, 2);
					V(2, 2) = RmatVar(2, 0);

					G(0, 0) = -0.25 / y / y*(R(0, 2) - R(2, 0));
					G(0, 1) = 0.25 / y;
					G(0, 2) = -0.25 / y;

					qw = (G%V%G.Transpose())(0, 0);
				}
				else if (fabs(z) > fabs(x) && fabs(z) > fabs(y) && fabs(z) > fabs(smallnumber))
				{
					if (sqrt(-R(0, 0) - R(1, 1) + R(2, 2) + 1.0) > 0.0)
					{
						CSMMatrix<double> G(1, 3, 0);
						CSMMatrix<double> V(3, 3, 0);

						V(0, 0) = RmatVar(0, 0);
						V(1, 1) = RmatVar(1, 1);
						V(2, 2) = RmatVar(2, 2);

						G(0, 0) = -0.5*0.5 / sqrt(-R(0, 0) - R(1, 1) + R(2, 2) + 1.0);
						G(0, 1) = -0.5*0.5 / sqrt(-R(0, 0) - R(1, 1) + R(2, 2) + 1.0);
						G(0, 2) = 0.5*0.5 / sqrt(-R(0, 0) - R(1, 1) + R(2, 2) + 1.0);

						qz = (G%V%G.Transpose())(0, 0);
					}

					CSMMatrix<double> G(1, 3, 0);
					CSMMatrix<double> V(3, 3, 0);

					//x = (0.25/z)*(R(0,2) + R(2,0));
					V(0, 0) = qz;
					V(1, 1) = RmatVar(0, 2);
					V(2, 2) = RmatVar(2, 0);

					G(0, 0) = -0.25 / z / z*(R(0, 2) + R(2, 0));
					G(0, 1) = 0.25 / z;
					G(0, 2) = 0.25 / z;

					qx = (G%V%G.Transpose())(0, 0);

					//y = (0.25/z)*(R(1,2) + R(2,1));
					V(0, 0) = qz;
					V(1, 1) = RmatVar(1, 2);
					V(2, 2) = RmatVar(2, 1);

					G(0, 0) = -0.25 / z / z*(R(1, 2) + R(2, 1));
					G(0, 1) = 0.25 / z;
					G(0, 2) = 0.25 / z;

					qy = (G%V%G.Transpose())(0, 0);

					//w = (0.25/z)*(R(1,0) - R(0,1));
					V(0, 0) = qz;
					V(1, 1) = RmatVar(1, 0);
					V(2, 2) = RmatVar(0, 1);

					G(0, 0) = -0.25 / z / z*(R(1, 0) - R(0, 1));
					G(0, 1) = 0.25 / z;
					G(0, 2) = -0.25 / z;

					qw = (G%V%G.Transpose())(0, 0);
				}
			}

			return CSMQuaternion(qw, qx, qy, qz);
		}

		///3D Transformation
		CSMQuaternion Transformation3D(CSMQuaternion P, CSMQuaternion R, CSMQuaternion T, double S)
		{
			return ((R % P % (R.Conjugate()))*S + T);
		}

		/// Get a quaternion rotating about an arbitrary axis
		CSMQuaternion RotationAboutArbitraryAixs(const double x, const double y, const double z, const double ang_rad)
		{
			//Rotation axis
			CSMQuaternion R(0.0, x, y, z);
			R.Normalize();
			//Rotation angle
			double half_ang = ang_rad * 0.5;
			R.qw() = cos(half_ang);
			R.qx() *= sin(half_ang);
			R.qy() *= sin(half_ang);
			R.qz() *= sin(half_ang);

			return R;
		}

		/// Get a rotated quaternion derived using rotation axis and angle
		CSMQuaternion RotateAboutArbitraryAixs(const double x, const double y, const double z, const double ang_rad, const CSMQuaternion& P)
		{
			//Rotation axis
			CSMQuaternion R = RotationAboutArbitraryAixs(x, y, z, ang_rad);
			//return a rotated quaternion
			return R.Rotate(P);
		}

		/// Reflected quaternion (vector)
		CSMQuaternion Reflection(const CSMQuaternion& P, const CSMQuaternion& normalVec)
		{
			CSMQuaternion P0 = P; P0.Normalize();
			CSMQuaternion normalVec0 = normalVec; normalVec0.Normalize();
			return (normalVec0 % P0 % normalVec0)*P.Norm();
		}

		void EulerToQuaternion(const CSMMatrix<double>& R, double& w, double& x, double& y, double& z)
		{
			try
			{
				w = 0.5*sqrt(R(0, 0) + R(1, 1) + R(2, 2) + 1.0);

				if (fabs(w) > fabs(smallnumber))
				{
					x = (0.25 / w)*(R(2, 1) - R(1, 2));
					y = (0.25 / w)*(R(0, 2) - R(2, 0));
					z = (0.25 / w)*(R(1, 0) - R(0, 1));
				}
				else
				{
					x = 0.5*sqrt(R(0, 0) - R(1, 1) - R(2, 2) + 1.0);
					y = 0.5*sqrt(-R(0, 0) + R(1, 1) - R(2, 2) + 1.0);
					z = 0.5*sqrt(-R(0, 0) - R(1, 1) + R(2, 2) + 1.0);

					if (fabs(x) >= fabs(y) && fabs(x) >= fabs(z) && fabs(x) > fabs(smallnumber))
					{
						y = (0.25 / x)*(R(0, 1) + R(1, 0));
						z = (0.25 / x)*(R(2, 0) + R(0, 2));
						w = (0.25 / x)*(R(2, 1) - R(1, 2));
					}
					else if (fabs(y) >= fabs(x) && fabs(y) >= fabs(z) && fabs(y) > fabs(smallnumber))
					{
						x = (0.25 / y)*(R(0, 1) + R(1, 0));
						z = (0.25 / y)*(R(1, 2) + R(2, 1));
						w = (0.25 / y)*(R(0, 2) - R(2, 0));
					}
					else if (fabs(z) > fabs(x) && fabs(z) > fabs(y) && fabs(z) > fabs(smallnumber))
					{
						x = (0.25 / z)*(R(0, 2) + R(2, 0));
						y = (0.25 / z)*(R(1, 2) + R(2, 1));
						w = (0.25 / z)*(R(1, 0) - R(0, 1));
					}
					else
					{
						std::string msg("Error: EulerToQuaternion(const CSMMatrix<double>& R) in SMQuaternion.h");
						std::cerr << msg;
						throw std::runtime_error(msg);
					}
				}
			}
			catch (...)
			{
				std::string msg("Error: EulerToQuaternion(const CSMMatrix<double>& R) in SMQuaternion.h");
				std::cerr << msg;
				throw std::runtime_error(msg);
			}
		}

		/// Error propagation for direction consines derived from Euler angles
		CSMMatrix<double> GetRmatVarfromEulerAngleVar(double omega, double phi, double kappa, const double VarO, const double VarP, const double VarK)
		{
			if (omega < 1.0e20)
				omega = 1.0e20;

			if (phi < 1.0e20)
				phi = 1.0e20;

			if (kappa < 1.0e20)
				kappa = 1.0e20;

			CSMMatrix<double> Varmat(3, 3, 0);
			CSMMatrix<double> G(1, 3);
			CSMMatrix<double> V(3, 3, 0);
			V(0, 0) = VarO;
			V(1, 1) = VarP;
			V(2, 2) = VarK;

			//Rmatrix(0,0) = cos(phi)*cos(kappa);
			G(0, 0) = 0;
			G(0, 1) = -sin(phi)*cos(kappa);
			G(0, 2) = -cos(phi)*sin(kappa);
			Varmat(0, 0) = (G%V%G.Transpose())(0, 0);

			//Rmatrix(1,0) = sin(omega)*sin(phi)*cos(kappa) + cos(omega)*sin(kappa);
			G(0, 0) = cos(omega)*sin(phi)*cos(kappa) - sin(omega)*sin(kappa);
			G(0, 1) = sin(omega)*cos(phi)*cos(kappa);
			G(0, 2) = -sin(omega)*sin(phi)*sin(kappa) + cos(omega)*cos(kappa);
			Varmat(1, 0) = (G%V%G.Transpose())(0, 0);

			//Rmatrix(2,0) = -cos(omega)*sin(phi)*cos(kappa) + sin(omega)*sin(kappa);
			G(0, 0) = sin(omega)*sin(phi)*cos(kappa) - cos(omega)*sin(kappa);
			G(0, 1) = -cos(omega)*cos(phi)*cos(kappa);
			G(0, 2) = -cos(omega)*cos(phi)*cos(kappa);
			Varmat(2, 0) = (G%V%G.Transpose())(0, 0);

			//Rmatrix(0,1) = -cos(phi)*sin(kappa);
			G(0, 0) = 0;
			G(0, 1) = sin(phi)*sin(kappa);
			G(0, 2) = -cos(phi)*cos(kappa);
			Varmat(0, 1) = (G%V%G.Transpose())(0, 0);

			//Rmatrix(1,1) = -sin(omega)*sin(phi)*sin(kappa) + cos(omega)*cos(kappa);
			G(0, 0) = -cos(omega)*sin(phi)*sin(kappa) - sin(omega)*cos(kappa);
			G(0, 1) = -sin(omega)*cos(phi)*sin(kappa);
			G(0, 2) = -sin(omega)*sin(phi)*cos(kappa) - cos(omega)*sin(kappa);
			Varmat(1, 1) = (G%V%G.Transpose())(0, 0);

			//Rmatrix(2,1) = cos(omega)*sin(phi)*sin(kappa) + sin(omega)*cos(kappa);
			G(0, 0) = -sin(omega)*sin(phi)*sin(kappa) + cos(omega)*cos(kappa);
			G(0, 1) = cos(omega)*cos(phi)*sin(kappa);
			G(0, 2) = cos(omega)*sin(phi)*cos(kappa) - sin(omega)*sin(kappa);
			Varmat(2, 1) = (G%V%G.Transpose())(0, 0);

			//Rmatrix(0,2) = sin(phi);
			G(0, 0) = 0;
			G(0, 1) = cos(phi);
			G(0, 2) = 0;
			Varmat(0, 2) = (G%V%G.Transpose())(0, 0);

			//Rmatrix(1,2) = -sin(omega)*cos(phi);
			G(0, 0) = -cos(omega)*cos(phi);
			G(0, 1) = sin(omega)*sin(phi);
			G(0, 2) = 0;
			Varmat(1, 2) = (G%V%G.Transpose())(0, 0);

			//Rmatrix(2,2) = cos(omega)*cos(phi);
			G(0, 0) = -sin(omega)*cos(phi);
			G(0, 1) = -cos(omega)*sin(phi);
			G(0, 2) = 0;
			Varmat(2, 2) = (G%V%G.Transpose())(0, 0);

			return Varmat;
		}
	}
}