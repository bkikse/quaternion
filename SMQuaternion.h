/*
 * Copyright (c) 2007-2017, KI IN Bang
 * All rights reserved.
 * SPATIAL&SPATIAL-MATICS for EarthOnAir
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS `AS IS'
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

#include "SMRotationMatrix.h"

namespace EOA
{
	const double smallnumber = 1.0e-11;

	enum CSMQuaternionError
	{
		_DIVIDED_ZERO_, //Divided by zero
		_NOT_POINT_ //the quaternion does not mean a normal 3D point (w is not zero).
	};

	//
	// Info about quaternion
	// Q1Q2 is not equal Q2Q1. Not commutative.
	// (Q1Q2)Q3 = Q1(Q2Q3)
	// (Q1+Q2)Q3 = Q1Q3 + Q2Q3
	// a*(Q1+Q2) = a*Q1 + a*Q2
	// (a*Q1)Q2 = Q1(a*Q2)
	//

	class CSMQuaternion
	{
	public:

		/// constructors
		CSMQuaternion();
		CSMQuaternion(const double w, const double x, const double y, const double z);
		/// constructor for a 3D vector
		CSMQuaternion(const double x, const double y, const double z);
		/// constructor from a rotation matrix
		CSMQuaternion(const CSMMatrix<double>& R);
		/// constructor for rotation axis and angle
		CSMQuaternion(const CSMQuaternion& V, const double th);
		/// copy constructor
		CSMQuaternion(const CSMQuaternion& copy);
		/// destructor
		~CSMQuaternion();

		//Operators

		/// assignment operator
		void operator = (const CSMQuaternion& copy);
		/// quaternion multiplication (Hamilton product) operator
		CSMQuaternion operator % (const CSMQuaternion& q) const;
		/// quaternion multiplication (Hamilton product) and assignment operator
		CSMQuaternion& operator %= (const CSMQuaternion& q);
		/// scalar multiplication operator
		CSMQuaternion operator * (const double& val) const;		
		/// quaternion division (non-commutative) operator
		CSMQuaternion operator / (const CSMQuaternion& q) const;		
		/// quaternion division (non-commutative) and assignment operator
		CSMQuaternion operator /= (const CSMQuaternion& q);
		/// scalar multiplication and assignment operator
		CSMQuaternion& operator *= (const double& val);
		/// quaternion dot (inner) product operator
		double operator * (const CSMQuaternion& val) const;
		/// scalar division operator
		CSMQuaternion operator / (const double& val) const;
		/// scalar division and assignment operator
		CSMQuaternion& operator /= (const double& val);
		/// quaternion addition operator
		CSMQuaternion operator + (const CSMQuaternion& q) const;
		/// quaternion addition and assignment operator
		CSMQuaternion& operator += (const CSMQuaternion& q);
		/// quaternion substraction operator
		CSMQuaternion operator - (const CSMQuaternion& q) const;
		/// quaternion substraction and assignment operator
		CSMQuaternion& operator -= (const CSMQuaternion& q);
		/// minus(-) sign operator
		CSMQuaternion operator - () const;
	
		double& qw();
		double& qx();
		double& qy();
		double& qz();
		void reset(const double w, const double x, const double y, const double z);

		/// conjugate quaternion
		CSMQuaternion Conjugate() const;
		/// 3D rotation
		CSMQuaternion Rotate(const CSMQuaternion P) const;
		/// 3D rotation
		CSMQuaternion Rotate(const double ax, const double ay, const double az) const;
		/// Partial derivatives of quaternion rotation w.r.t. "w"
		CSMQuaternion Rotate_dw(const double& ax, const double& ay, const double& az) const;
		/// Partial derivatives of quaternion rotation w.r.t. "x"
		CSMQuaternion Rotate_dx(const double& ax, const double& ay, const double& az) const;
		/// Partial derivatives of quaternion rotation w.r.t. "y"
		CSMQuaternion Rotate_dy(const double& ax, const double& ay, const double& az) const;
		/// Partial derivatives of quaternion rotation w.r.t. "z"
		CSMQuaternion Rotate_dz(const double& ax, const double& ay, const double& az) const;
		/// sum of squares
		double Norm2() const;
		/// norm
		double Norm() const;
		/// normalization (unit quaternion)
		void Normalize();	
		/// multiplicative identity quaternion
		/// (1, 0, 0, 0)%(w,x,y,z) = (w,x,y,z)
		/// (w,x,y,z)%(1, 0, 0, 0) = (w,x,y,z)
		void identity();

	private:
		double w, x, y, z;
	};

	namespace Rotation
	{
		/// quaternion to rotation matrix
		CSMMatrix<double> QuaternionToEuler(const double w, const double x, const double y, const double z);
		/// unit quaternion to rotation matrix
		CSMMatrix<double> UnitQuaternionToEuler(const double q0, const double q1, const double q2, const double q3);
		/// quaternion to Euler angles; depends on the order of rotations.
		void quatToEulerAngles(const double q0, const double q1, const double q2, const double q3, double& ang1, double& ang2, double& ang3);
		/// spherical quaternion interpolation
		CSMQuaternion Interpolation(const CSMQuaternion& qa, const CSMQuaternion& qb, const double ta, const double tb, const double t, const double th = 0.95);
		/// quaternion interpolation: slerp and lerp
		CSMQuaternion Interpolation(const CSMQuaternion& qa, const CSMQuaternion& qb0, const double u, const double th = 0.95);
		/// interpolation: slerp
		CSMQuaternion InterpolationSlerp(const CSMQuaternion& qa, const CSMQuaternion& qb0, const double dot0, const double u);
		/// lerp quaternion interpolation
		CSMQuaternion InterpolationLerp(const CSMQuaternion& qa, const CSMQuaternion& qb, const double u);
		/// quaternion interpolation; it does not check > 90 deg
		CSMQuaternion InterpolationNoInvert(const CSMQuaternion& qa, const CSMQuaternion& qb, const double u, const double th = 0.95);
		/// error propagation for quaternion derived from Euler angles
		CSMQuaternion EulerToQuaternion_Variance(const double omega, const double phi, const double kappa, const double VarO, const double VarP, const double VarK);
		/// 3D Transformation
		CSMQuaternion Transformation3D(CSMQuaternion P, CSMQuaternion R, CSMQuaternion T, double S);
		/// get a quaternion rotating about an arbitrary axis
		CSMQuaternion RotationAboutArbitraryAixs(const double x, const double y, const double z, const double ang_rad);
		/// get a rotated vector(quaternion) derived using rotation axis and angle
		CSMQuaternion RotateAboutArbitraryAixs(const double x, const double y, const double z, const double ang_rad, const CSMQuaternion& P);
		/// reflected quaternion (vector)
		CSMQuaternion Reflection(const CSMQuaternion& P, const CSMQuaternion& normalVec);
		/// Euler angles to quaternion
		void EulerToQuaternion(const CSMMatrix<double>& R, double& w, double& x, double& y, double& z);
		/// error propagation for direction consines derived from Euler angles
		CSMMatrix<double> GetRmatVarfromEulerAngleVar(double omega, double phi, double kappa, const double VarO, const double VarP, const double VarK);
	}
}